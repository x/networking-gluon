===============================
networking-gluon
===============================

This Service Plugin is a framework to implement Gluon functionality in neutron

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/networking-gluon
* Source: http://git.openstack.org/cgit/openstack/networking-gluon
* Bugs: http://bugs.launchpad.net/networking-gluon

Features
--------

* TODO
